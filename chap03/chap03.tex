%begin-include
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : chap03.tex
% 	Modif : sam. 23 déc. 2023 17:57
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\Chapitre{Le changement dans la continuité avec git}

\Section{Parler de git sans me répéter}

J'ai déjà parlé de git et de gitlab dans deux de mes documents qui ne parlent pas de vélo.
À la fois dans le \Urlref{https://scarpet42.gitlab.io/lagivim/lagivim.pdf}{document sur \LaTeX{}} et dans \Urlref{https://scarpet42.gitlab.io/dddk/dddk.pdf}{celui sur Docker et Kubernetes}.
C'est vraiment important, pour deux raisons~:
\begin{tuxliste}
\item Quelle que soit la méthode de développement, git est important.
\item L'apport de gitlab par rapport à git reflète le passage du cycle en V à la méthode agile.
\end{tuxliste}

Je vais donc devoir en parler ici encore.
En essayant de ne pas trop me répéter.
Je vais donc essayer de l'aborder sous un autre angle.
D'abord, git, c'est un gestionnaire de code, il en existe plusieurs autres et en soi ce n'est pas lié à l'agilité.
Dès que vous écrivez quelque chose qui est long à écrire, qui va évoluer et qui est pérenne, vous devez pouvoir gérer son historique.
Si vous êtes plusieurs à le faire évoluer, c'est à dire si vous travaillez en équipe, c'est encore plus important.

Pour la gestion du code source des applications, il est donc utile d'utiliser un \Definition{c}{CVS}{pour Concurrent Versions System, est un gestionnaire de version de code}.
Il y en a plusieurs, comme subversion, git ou Mercurial.
Pour plein de raisons, il y a des années, après beaucoup de recherches, j'ai opté pour git.
Je ne suis pas un expert, je l'utilise de façon basique pour mon usage personnel.

Le principe, c'est que les développeurs ont tous la totalité des sources sur leur ordinateur.
En local, ils créent chacun leur propre branche d'historique.
Ils font les modifications sur le code source qui est dans leur branche.
Régulièrement, ils envoient le contenu de leurs modifications sur le dépôt central.
Une fois que leur code est finalisé, leur branche est incluse dans la branche principale et ils peuvent créer une autre branche pour développer une autre fonctionnalité.
C'est git qui se charge de l'historique des versions, d'éviter que le code de l'un ne fasse disparaître le code de l'autre et ainsi de suite.
Voilà, il n'y a de rapport entre ce besoin de gestion de code et la méthode de travail.
Même l'institut de la \Urlref{http://www.la-rache.com/}{RACHE} utilise git (ils parlent de leur dépôt github dans les news).

Comme les développeurs savent coder, quand une tâche est répétitive, ils la codent et gagnent du temps.
C'est pour ça que le clavier est très supérieur à la souris dans beaucoup de cas.
C'est pour ça que même Microsoft qui a essayé d'imposer le concept de «~tout-souris~» sur Windows a dû abandonner l'idée.
Pendant des années, Windows avait une invite de commandes toute pourrie.
Elle était tellement pourrie qu'ils n'ont pas essayé de l'améliorer et on dû créer le PowerShell.
Personnellement, je ne l'ai jamais utilisé, je ne sais pas ce que ça vaut mais il paraît que ce n'est pas si mal.
Puisque je ne veux pas être polémique sur l'agilité versus le cycle en V, je vais me défouler en disant que je doute fortement qu'ils aient pu faire quelque chose aussi performant que les shells dignes de ce nom qui tournent sous Linux.
En tous cas, ça prouve que le besoin de contourner les limitations de la souris était très important.

\Section{L'automatisation traditionnelle selon git}

Personnellement, chez moi, j'ai donc git.
Et puisque vous pouvez voir ce document, c'est que vous avez pu le récupérer sur mon dépôt gitlab.
J'utilise gitlab à la fois comme un moyen de sauvegarde et comme un moyen de mise à disposition des fichiers.

D'une façon générale, git propose des possibilités d'automatiser des actions en fonction des mises à jour du code.
Pour le voir, il suffit de regarder les fichiers présents dans le répertoire hooks.

\begin{listbash}
(stef)>ls latex/ziva/.git/hooks/
applypatch-msg.sample post-update.sample pre-merge-commit.sample pre-rebase.sample
update.sample commit-msg.sample pre-applypatch.sample prepare-commit-msg.sample
pre-receive.sample fsmonitor-watchman.sample pre-commit.sample pre-push.sample
push-to-checkout.sample
\end{listbash}

Les noms parlent d'eux-mêmes.
Si vous ne comprenez pas à quoi ils correspondent, ce n'est pas très important.
Les explications seraient techniques et nous entraineraient trop loin.
Ce que vous devez comprendre, c'est juste que ce sont des scripts shell qu'il suffit de renommer pour utiliser.
Si vous ne savez pas ce que sont des scripts shell, disons que ce sont des programmes dont le code est compréhensible par la ligne de commande.
Au lieu de tapper tout un tas de commandes une par une, il suffit de les écrire dans l'un des fichiers, de renommer le fichier en enlevant le «~.sample~» et c'est tout.
Lorsqu'une action est faite avec git, si l'un des fichiers a un nom qui va bien, il est automatiquement exécuté.
Si les fichiers sont présents par défaut, c'est parce qu'ils contiennent déjà des commandes utilisables par défaut.

Vous pouvez voir qu'il y a des «~.sample~» tout partout et que je ne les utilise donc pas.
D'abord, je pourrais automatiser le transfert des fichiers sur mon ordinateur vers mon dépôt gitlab.
Cependant, comme gitlab a des automatismes, je préfère contrôler ce que j'envoie sur mon dépôt et éviter des mises à jour intempestives.
Ensuite, j'ai utilisé avec succès pendant des années la compilation et la mise à disposition automatique de mes fichiers pdf sur un site Internet.
C'est ce que je faisais lorsque mon site Internet était hébergé sur mon ordinateur.
Je ne sais pas si gitlab est capable d'utiliser les hooks internes de git.
Comme il est préférable d'utiliser les possibilités offertes par gitlab que de tout refaire moi-même, je n'ai pas cherché à le savoir.
Depuis que j'utilise gitlab j'ai donc arrêté d'utiliser mes personnalisations des hooks internes à git.

Vous avez le droit de vous demander pourquoi je parle des hooks internes de git que je n'utilise plus.
Je vous rassure, ce n'est pas par nostalgie.
La raison, c'est que les automatismes internes à git sont des programmes standards dont la philosophie est différente de celle de gitlab.
Mais le plus important, c'est que la philosophie des hooks de git reflète celle des cycles en V alors que la philosophie des automatisations de gitlab reflète celle de l'agilité.
C'est pour ça que ça me semble une bonne opportunité de s'y attarder.
Parce que ce changement possède l'avantage de son inconvénient.
Ou l'inconvénient de son avantage, au choix.
L'avantage, c'est qu'une fois maîtrisée, la chaine d'automatisation apporte une très grande puissance et une très grande souplesse.
L'inconvénient, c'est que sa maîtrise est très compliquée, j'y reviendrai plus tard.

Pour l'instant, regardons très rapidement les hooks traditionnels de git.
C'est vraiment du développement de base comme n'importe quel langage de programmation.
Vous avez donc toute la puissance d'un vrai langage de programmation.
Dit autrement, tout ce qui peut être fait avec les automatismes de gitlab peut être fait avec les hooks de git.
C'est juste la façon de faire qui est fondamentalement différente, pas le résultat.
Ça peut être très long et très compliqué à faire, mais n'importe quel développeur peut suivre le code pour comprendre ce qu'il fait.

Le principe est classique, je ne vais pas m'y attarder.
Vous prescrivez à git ce qu'il doit faire.
Vous lui donnez les étapes, une par une pour qu'il fasse ce que vous voulez.
C'est la prescription qui est importante ici.
C'est comme dans le cycle en V.
Si dans le cycle en V vous avez plusieurs niveaux de documentation, c'est parce que vous avez des personnes différentes qui interviennent à différents moments du travail.
Pour la maison, vous vous adressez à l'architecte qui s'adresse au chef de chantier qui s'adresse au chef d'équipe qui dit à l'ouvrier ce qu'il doit faire.
L'ouvrier n'a aucune marge de manœuvre, il a juste à obéir et à faire ce qui lui est prescrit.
Et c'est donc fondamentalement différent de l'agilité et de l'automatisation de gitlab.
Pour comprendre en quoi les autres méthodes sont différentes, faisons une étape intermédiaire.

\Section{Explications sur la structure des fichiers yaml}

Pour simplifier la suite, je vais être un peu obligé de faire semblant d'expliquer de la technique.
Le \Definition{c}{YAML}{pour Yaml Ain't Markup Language, ou Yaml n'est pas un langage de balises en français, est un langage de description de données}.
Ce n'était pas sa définition initiale, l'emphase actuelle est mise sur le contenu.
Le but n'est pas de gérer des documents mais bien des données.

C'est facile à utiliser, je vais vous faire la description complète de tout ce qui est nécessaire pour comprendre la suite.

\begin{monfichier}{yaml}{commentaires.yml}
# D'abord, il y a les commentaires.
# Je peux écrire ce que je veux dedans, ce n'est pas interprété par les décodeurs yaml.
# C'est pratique, ça permet d'expliquer la raison de certaines valeurs.
\end{monfichier}

Une fois que je peux mettre des commentaires pour expliquer le contenu du fichier, les explications sont plus simples à donner.
En YAML, il n'y a que deux types de données.
Les scalaires et les listes.
Les données sont organisées par indentations~: il ne faut pas utiliser les tabulations, mais les espaces.
S'il y a le même nombre d'espaces au début de la ligne, c'est que ça se rapporte au même élément défini avec moins d'indentations.
J'ai du mal à voir comment mieux expliquer.
Regardons un fichier en commençant avec les scalaires, ce sera plus clair.

\begin{monfichier}{yaml}{scalaires.yml}
# Commençons par définir un beau vélo
# Vous pouvez voir que ce sont les roues qui sont au nombre de deux et dont la taille fait 700mm
# Alors que ce sont les pneus du vélo qui font 25mm
# Vous voyez aussi que c'est le vélo qui est de couleur champagne et or.
# Ce ne sont ni les roues ni les pneus
velo:
   roues:
      nombre: 2
      taille: 700mm
   pneus: 25mm
   couleur: champagne et or
   vitesses:
      nombre: 20
      type: indexées
   guidon: route
   freins: patins

# Le nombre d'espaces n'est pas très important tant que tout est bien aligné.

# En mettant trois traits d'union, il est possible de définir un autre fichier
---
# Là, quand cette partie sera traitée automatiquement elle sera considérée comme un autre fichier.
# C'est juste un choix de gestion pour éviter d'avoir trop de petits fichiers.
ordinateur:
   composants:
      type: laptop
      cpu: 8 cœurs
      RAM: 16Go
      clavier: français
# Avec l'indentation, vous voyez que c'est le système d'exploitation de l'ordinateur
# pas du composant
   systeme_exploitation:
      type: Linux
      distribution: ArchLinux
\end{monfichier}

Le deuxième type est la liste, là, c'est pour définir les éléments, soyons fous, d'une liste.
Pareil, partons d'un exemple.

\begin{monfichier}{yaml}{liste.yml}
# Tout le monde connaît les trois mousquetaires :
trois_mousquetaires:
   - Aramis
   - Athos
   - Porthos
---
# Il est possible de tout combiner.
# Vous avez une liste de trois petits cochons avec leurs caractéristiques
trois_petits_cochons:
   - numéro: 1
     esprit: insouciant
     maison: paille
     résultat: perdu
   - numéro: 2
     résultat: perdu
     maison: bois
     esprit: moins insouciant
   - numéro: 3
     esprit: malin
     résultat: gagné
     maison: briques et ciment
# J'ai mis les options des listes dans des ordres différents pour montrer que l'ordre des ligne n'est pas important.
\end{monfichier}

Voilà, vous voyez bien que ce n'est pas très compliqué à comprendre.
C'était le but initial d'être facilement compréhensible par un être humain.
La difficulté est de rédiger le fichier, pas de le comprendre.
Bon, là, je mettais ce que je voulais, mais dans le cas d'un fichier destiné à être interprété par un ordinateur, il faut connaître les valeurs possibles.
Puisque les cochons ont presque tous été mangés, nous pouvons en revenir à nos moutons.

\Section{Changement de paradigme avec YAML}

Pour en revenir au MVP, regardons la différence entre ce qui pourrait être écrit en script et ce qui pourrais être écrit en YAML.
D'abord, ce que pourrait donner un script très simplifié.

\begin{monfichier}{yaml}{méthode prescriptive}
# Directives données aux ouvriers
- Construisez 4 murs
- Posez un toit dessus
- Installez une porte
- Installez une prise électrique
- Installez un lavabo
- Installez des toilettes
\end{monfichier}

Bien sûr, c'est un schémas pour avoir un ordre d'idée, il faut donner plus de précisions, comme l'endroit où poser la porte par exemple.
Alors que la même chose décrite en YAML donnerait quelque chose comme~:

\begin{monfichier}{yaml}{méthode descriptive}
# Description du MVP de la maison
maison:
   murs: 4
   toits: 1
   porte: 1
   prises_electriques: 1
   lavabo: 1
   toilettes: 1
\end{monfichier}

Là aussi, le but est surtout d'illustrer la différence entre les deux méthodes.
Comme je le disais avant de présenter le YAML, dans la méthode de gestion de projets traditionnelle, il y a plusieurs intermédiaires entre  le client final et l'ouvrier.
Alors que l'agilité demande des interactions entre le client final et l'ouvrier.
Comme le client final ne connaît pas le métier de l'ouvrier, il ne peut pas lui dire comment faire son travail.
C'est donc à l'ouvrier de savoir ce qu'il doit faire pour répondre à votre demande.
Votre travail consiste à définir votre besoin, vous ne vous occupez plus de la façon d'y répondre.

Vous voyez qu'avec le passage du cycle en V à l'agilité, vous passez de la méthode prescriptive à la méthode descriptive.
Et pour le passage des scripts shell de git aux fichiers YAML de gitlab c'est exactement pareil.
Je donne des commandes et des conditions à gitlab et c'est lui qui se charge de savoir ce qu'il doit exécuter et quand l'exécuter.
Je me suis posé la question de savoir si je mettais mon fichier \liglat{.gitlab-ci.yml} ici ou pas.
Comme c'est assez technique et que je l'ai déjà montré dans \Urlref{https://scarpet42.gitlab.io/dddk/dddk.pdf}{dddk} je ne vais pas me répéter.
Vous pouvez aller le voir si vous êtes intéressés.

La difficulté réside dans la rédaction du fichier plus que dans sa compréhension.
Il ne suffit pas de réussir à le comprendre, il faut réussir à le rédiger.
C'est à dire qu'il faut connaître toutes les options nécessaires et les valeurs qu'elles doivent prendre.
Pour un expert c'est facile, mais c'est souvent très difficile d'être un expert.

Parce que le script, il commence au début et il se déroule dans un ordre chronologique.
Il peut y avoir des boucles et des conditions, mais c'est toujours possible à suivre.
Donc, si quelque chose se passe mal, il suffit de regarder à quel endroit le script ne fait pas ce qui est attendu et de le corriger.
Bon, c'est pas toujours évident de savoir où un script complexe échoue, mais il y quand même une certaine linéarité.
Alors que le fichier yaml, il faut savoir quoi mettre dedans.
Et si le résultat n'est pas celui attendu, il faut vérifier les paramètres aux endroits les plus probables.
Mais il n'y a pas un ordre linéaire.
C'est possible que ce soit le dernier paramètre du fichier qui soit mauvais et qui pose un problème dès le début.
Comme nous ne savons pas comment est traité le fichier, c'est très compliqué de savoir ce qui se passe mal.

Un dernier détail, sur gitlab, comme évoqué plus haut, le fichier \liglat{.gitlab-ci.yml} est le fichier qui est utilisé par les automatismes de gitlab.
C'est ce qui s'appelle la chaine \Definition{c}{CI/CD}{pour Continuous Integration, Continuous Development, Continuous Deployment, ou en français, Intégration Continue, Développement Continu, Déploiement Continu, dont le principe est de permettre au développeur de coder et de ne pas s'occuper du reste}.
Le développeur développe et, grace à git, envoie son code sur le dépôt, puis recommence à coder.
Il ne s'occupe de rien d'autre, c'est central à tous les outils dont je vais parler ensuite.
Ça impacte les équipes techniques et nous allons donc voir comment avec les autres outils à étudier.

%end-include
