# Ziva Impactons Vite l'Agilité

Ce projet est destiné à produire un document LaTeX.

## Le but du projet est de produire un document expliquant les impacts de l'agilité

Aujourd'hui, le sujet à la mode est la mise en place de l'agilité dans les entreprises.
Toutes les formations se focalisent sur l'utilisation de l'agilité.
C'est un état d'esprit, c'est vrai.
Mais les impacts sont réels et trop rarement expliqués.
Ils sont donc souvent mal anticipés et donc mal acceptés.
Par l'équipe et par ceux qui travaillent avec cette équipe.

Je vais chercher à comprendre comment l'agilité impacte les façons de travailler et comment c'est prit en compte.
Je ne vais pas faire une étude complète de tout ce qui existe.
Pour chaque problème, il existe des produits concurents qui y répondent.
Je vais donc partir des problèmes et regarder leurs solutions.
Le document n'est pas technique, il est plus destinés aux décideurs qu'aux techniciens qui implémentent les solutions.
De façon à ce que je puisse comprendre les choix qui me sont proposés dans le futur.


## Lecture du document compilé
Le document compilé est disponible ici : [ziva.pdf](https://scarpet42.gitlab.io/ziva/ziva.pdf)

## License

La licence est la WTFPL : [WTFPL](http://www.wtfpl.net/)
C'est la plus permissive des licences.
Si je ne le fais pas, vous n'avez rien le droit de faire du texte sans mon accord.
